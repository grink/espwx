#include "settings.h" // Contains private config data
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266WebServer.h>
#include <Adafruit_BME280.h>
#include <LaCrosse_TX23.h>
#include <Wire.h>
#include <ArduinoJson.h>

// I2C pins for BME280
#define SDA 4
#define SCL 5

// Rain bucket pin
#define RAINPIN 12

// TX23U pin
#define TX23UPIN 13

// Voltage divider equation VOLTM*adc+VOLTX
#define VOLTM 5.099
#define VOLTX -0.005

// ** Defined in settings.h
const char* ssid = WIFISSID;
const char* password = WIFIPASSWORD;
const char* host = HOST;
const char* otapw = OTAPW;
// **

// Store when the code was compiled
const char* compile_date = __DATE__ " " __TIME__;

int rain_count = 0;
int bme_detected = 0;
unsigned long rain_time = 0;
unsigned long last_rain_time = 0;

Adafruit_BME280 bme;
LaCrosse_TX23 anemometer = LaCrosse_TX23(TX23UPIN);
ESP8266WebServer server(80);

void handleRoot() {
  server.send(200, "text/plain", "Welcome to the ESP8266 weather station!");
}

String takeReading() {
  StaticJsonDocument<1000> doc;
  JsonObject& weather = doc.to<JsonObject>();
  String jsonStr = "";
  float adc;
  if (bme_detected) {
    bme.takeForcedMeasurement();
    weather["temperature"] = bme.readTemperature();
    weather["temperature_unit"] = "C";
    weather["humidity"] = bme.readHumidity();
    weather["humidity_unit"] = "%";
    weather["pressure"] = bme.readPressure() / 100.0F;
    weather["pressure_unit"] = "hPa";
  }
  String dirTable[] = {"N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"};
  float speed;
  int direction;
  if (anemometer.read(speed, direction))
  {
    weather["windspeed"] = String(speed, 1);
    weather["windspeed_unit"] = "m/s";
    weather["winddirection"] = dirTable[direction];
  }
  weather["rain"] = rain_count;
  weather["rain_unit"] = "counter";
  weather["uptime"] = (millis() / (1000 * 60));
  weather["uptime_unit"] = "minutes";
  weather["rssi"] = WiFi.RSSI();
  adc = (float(analogRead(A0)) / 1024) * 1.06;
  weather["battery_volts"] = VOLTM * adc + VOLTX;
  weather["adc_raw"] = adc;
  weather["compiledate"] = compile_date;
  serializeJsonPretty(weather, jsonStr);
  return jsonStr;
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void rain() {
  rain_time = millis();
  // debounce
  if (rain_time - last_rain_time > 250) // TODO: make this shorter
  {
    rain_count++;
    last_rain_time = rain_time;
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  pinMode(RAINPIN, INPUT_PULLUP);
  attachInterrupt(RAINPIN, rain, FALLING);
  interrupts();
  Wire.begin(SDA, SCL);
  if (bme.begin()) {
    bme_detected = 1;
    bme.setSampling(Adafruit_BME280::MODE_FORCED,
                    Adafruit_BME280::SAMPLING_X4, // temperature
                    Adafruit_BME280::SAMPLING_X4, // pressure
                    Adafruit_BME280::SAMPLING_X4, // humidity
                    Adafruit_BME280::FILTER_OFF   );
  }

  WiFi.mode(WIFI_STA);
  WiFi.hostname(host);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    delay(5000);
    ESP.restart();
  }
  // *** OTA begin
  ArduinoOTA.setPassword(otapw);
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  // *** OTA end

  server.on("/", handleRoot);
  server.on("/reboot", []() { // TODO: add auth
    ESP.restart();
  });
  server.on("/sensor", []() {
    server.send(200, "text/plain", takeReading());
  });
  server.onNotFound(handleNotFound);
  server.begin();
}

void loop() {
  ArduinoOTA.handle();
  server.handleClient();
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(5000);
    ESP.restart();
  }
}
